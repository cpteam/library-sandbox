<?php declare(strict_types = 1);

const LIBRARY_JSON = __DIR__ . '/library.json';
const LIBRARY_LOCK = __DIR__ . '/library.lock';

const LIBRARY_STORAGE = __DIR__ . '/libs/';

$temp = __DIR__ . '/tmp' . time();

require __DIR__ . '/vendor/autoload.php';

/**
 * @return mixed
 */
function readJson($path)
{
	return \Nette\Utils\Json::decode(file_get_contents($path), \Nette\Utils\Json::FORCE_ARRAY);
}

$lock = [
	'repository' => [

	],
];

$json = readJson(LIBRARY_JSON);

mkdir($temp, 0775, true);

$packages = $json['packages'];

if (file_exists(LIBRARY_STORAGE) === false) {
	mkdir(LIBRARY_STORAGE, 0775, true);
} else {
	unlink(LIBRARY_STORAGE);
	mkdir(LIBRARY_STORAGE, 0775, true);
}

foreach($packages as $package) {
	$packagePath = $temp . '/' . md5($package);
	mkdir($packagePath, 0775, true);

	$cmd = "git clone -b develop $package $packagePath";
	e($cmd);
	shell_exec($cmd);

	if (file_exists($packagePath . '/composer.json') === false) {
		throw new \Exception("$package doesn't contain composer.json at root dir");
	}

	$settings = readJson($packagePath . '/composer.json');

	if (isset($settings['name']) === false) {
		throw new \Exception("$package doesn't contain name in composer.json");
	}

	$packageNameReal = $settings['name'];
	$packageName = str_ireplace('/', '-', $packageNameReal);

	rename($packagePath, LIBRARY_STORAGE . '/' . $packageName);

	$lock['repository'][$packageNameReal] = [
		'name' => $packageNameReal,
		'path' => LIBRARY_STORAGE . '/' . $packageName,
	];
}

unlink($temp);

file_put_contents(LIBRARY_LOCK, \Nette\Utils\Json::encode($lock));




function e($m) {
	echo $m . PHP_EOL;
}
