#!/usr/bin/env bash


getClone() {
	git clone -b develop git@gitlab.com:cpteam/$1 libs/CPTeam/$1
}

getClone console
getClone core
getClone deploy
getClone forms
getClone image
getClone nette
getClone og
getClone package
getClone security


echo "Copy idea files"
cp vcs.xml .idea/vcs.xml
